import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Todo App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void createRecord() async {
    Firestore.instance.runTransaction((transaction) async {
      return await transaction
          .set(Firestore.instance.collection("todo").document(), {"title":title.text,"text":text.text,});
    });
  }

  final databaseReference = Firestore.instance;
  var _listSection = List<Widget>();
  TextEditingController title = new TextEditingController();
  TextEditingController text = new TextEditingController();

  void getData() {
    databaseReference
        .collection("todo")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f) => setState(() {_addListTile(f.data['title'],f.data['text'] );}));
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Padding(
            padding: EdgeInsets.all(10),
            child: new Column(
              children: <Widget>[
                TextField(
                    controller: title,
                    decoration: InputDecoration(
                      labelText: 'Title ',
                    )),
                TextField(
                  controller: text,
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  decoration: InputDecoration(
                    labelText: 'Description',
                  ),
                ),
                SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: RaisedButton(
                        onPressed: () {
                          createRecord();
                          setState(() {
                            _addListTile(title.text, text.text);
                          });
                          Navigator.pop(context);
                        },
                        color: Colors.blue,
                        child: const Text(
                          'Submit Button',
                          style: TextStyle(color: Colors.white),
                        ))),
              ],
            ),
          );
        });
  }

  void _addListTile(String title, String text) {
    _listSection.add(ListTile(
        leading: Icon(Icons.add_circle),
        title: Text(title),
        subtitle: Text(text)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          Column(
            children: this._listSection, // ----> Add this
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _settingModalBottomSheet(context);
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
